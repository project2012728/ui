    function submitForm(event) {
      event.preventDefault();
      var form = document.getElementById("callback-form");
      form.style.display = "none";

      var thankYouMessage = document.createElement("h2");
      thankYouMessage.textContent = "Thank you! Your appointment has been booked.";
      thankYouMessage.style.color = "#5348f3";
      document.body.appendChild(thankYouMessage);
    }
