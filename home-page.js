let Hyderabad = document.getElementById('hyderabad');
let Bangalore = document.getElementById('bangalore');
let Delhi = document.getElementById('delhi');
let Chennai = document.getElementById('chennai');
let Mumbai = document.getElementById('mumbai');
let Kolkata = document.getElementById('kolkata');


function showBranchH() {
    Hyderabad.innerHTML = "";

    let divelement = document.createElement('div');

    let image = document.createElement('img');
    image.classList.add('shadow-lg', 'sa');
    image.setAttribute('src', 'https://cdn.apollohospitals.com/dev-apollohospitals/2023/06/apollo-hyderabad-2.jpg');
    image.setAttribute('alt', 'img is not found');

    let head = document.createElement('h3');
    const headTextNode = document.createTextNode("Hospitals in Hyderabad");
    head.appendChild(headTextNode);

    let para = document.createElement('p');
    let icon1 = document.createElement('i');
    icon1.classList.add('fa-solid', 'fa-location-dot');
    para.appendChild(icon1);

    let paraTextNode = document.createTextNode("Film Nagar, Jubilee Hills, Hyderabad, Telangana State, India. Toll No  1860 500 1066");
    para.appendChild(paraTextNode);

    let para1 = document.createElement('p');
    let icon2 = document.createElement('i');
    icon2.classList.add('fa-solid', 'fa-phone');
    para1.appendChild(icon2);

    paraTextNode = document.createTextNode(" +91-40-2360 7777 / 5555 / 2000");
    para1.appendChild(paraTextNode);

    let icon3 = document.createElement('i');
    icon3.classList.add('fa-solid', 'fa-location-arrow');
    para1.appendChild(icon3);

    let icon4 = document.createElement('i');
    icon4.classList.add('fa-solid', 'fa-envelope');
    para1.appendChild(icon4);

    divelement.appendChild(image);
    divelement.appendChild(head);
    divelement.appendChild(para);
    divelement.appendChild(para1);

    Hyderabad.appendChild(divelement);
}

function showBranchB() {
    Bangalore.innerHTML = "";

    let divelement = document.createElement('div');

    let image = document.createElement('img');
    image.classList.add('shadow-lg', 'sa');
    image.setAttribute('src', 'https://cdn.apollohospitals.com/dev-apollohospitals/2023/06/apollo-bangalore-01.jpg');
    image.setAttribute('alt', 'img is not found');

    let head = document.createElement('h3');
    const headTextNode = document.createTextNode("Hospitals in Bangalore");
    head.appendChild(headTextNode);

    let para = document.createElement('p');
    let icon1 = document.createElement('i');
    icon1.classList.add('fa-solid', 'fa-location-dot');
    para.appendChild(icon1);

    let paraTextNode = document.createTextNode("154 / 11, Bannerghatta Road Opp. I.I.M Bangalore  560 076");
    para.appendChild(paraTextNode);

    let para1 = document.createElement('p');
    let icon2 = document.createElement('i');
    icon2.classList.add('fa-solid', 'fa-phone');
    para1.appendChild(icon2);

    paraTextNode = document.createTextNode(" +(91)-80-2630 4050");
    para1.appendChild(paraTextNode);

    let icon3 = document.createElement('i');
    icon3.classList.add('fa-solid', 'fa-location-arrow');
    para1.appendChild(icon3);

    let icon4 = document.createElement('i');
    icon4.classList.add('fa-solid', 'fa-envelope');
    para1.appendChild(icon4);

    divelement.appendChild(image);
    divelement.appendChild(head);
    divelement.appendChild(para);
    divelement.appendChild(para1);

    Bangalore.appendChild(divelement);
}

function showBranchD() {
    Delhi.innerHTML = "";

    let divelement = document.createElement('div');

    let image = document.createElement('img');
    image.classList.add('shadow-lg', 'sa');
    image.setAttribute('src', 'https://cdn.apollohospitals.com/dev-apollohospitals/2020/09/apollo-delhi.png');
    image.setAttribute('alt', 'img is not found');

    let head = document.createElement('h3');
    const headTextNode = document.createTextNode("Hospitals in Delhi");
    head.appendChild(headTextNode);

    let para = document.createElement('p');
    let icon1 = document.createElement('i');
    icon1.classList.add('fa-solid', 'fa-location-dot');
    para.appendChild(icon1);

    let paraTextNode = document.createTextNode("Sarita Vihar Delhi Mathura Road, New Delhi  110076 (India)");
    para.appendChild(paraTextNode);

    let para1 = document.createElement('p');
    let icon2 = document.createElement('i');
    icon2.classList.add('fa-solid', 'fa-phone');
    para1.appendChild(icon2);

    paraTextNode = document.createTextNode("Call Centre(24X7): +91-11-71791090 / 1091");
    para1.appendChild(paraTextNode);

    let icon3 = document.createElement('i');
    icon3.classList.add('fa-solid', 'fa-location-arrow');
    para1.appendChild(icon3);

    let icon4 = document.createElement('i');
    icon4.classList.add('fa-solid', 'fa-envelope');
    para1.appendChild(icon4);

    divelement.appendChild(image);
    divelement.appendChild(head);
    divelement.appendChild(para);
    divelement.appendChild(para1);

    Delhi.appendChild(divelement);
}

function showBranchC() {
    Chennai.innerHTML = "";

    let divelement = document.createElement('div');

    let image = document.createElement('img');
    image.classList.add('shadow-lg', 'sa');
    image.setAttribute('src', 'https://cdn.apollohospitals.com/dev-apollohospitals/2023/06/apollo-chennai.jpg');
    image.setAttribute('alt', 'img is not found');

    let head = document.createElement('h3');
    const headTextNode = document.createTextNode("Hospitals in Chennai");
    head.appendChild(headTextNode);

    let para = document.createElement('p');
    let icon1 = document.createElement('i');
    icon1.classList.add('fa-solid', 'fa-location-dot');
    para.appendChild(icon1);

    let paraTextNode = document.createTextNode("21, Greams Lane, Off Greams Road Chennai – 600006");
    para.appendChild(paraTextNode);

    let para1 = document.createElement('p');
    let icon2 = document.createElement('i');
    icon2.classList.add('fa-solid', 'fa-phone');
    para1.appendChild(icon2);

    paraTextNode = document.createTextNode(" +91-44-40401066");
    para1.appendChild(paraTextNode);

    let icon3 = document.createElement('i');
    icon3.classList.add('fa-solid', 'fa-location-arrow');
    para1.appendChild(icon3);

    let icon4 = document.createElement('i');
    icon4.classList.add('fa-solid', 'fa-envelope');
    para1.appendChild(icon4);

    divelement.appendChild(image);
    divelement.appendChild(head);
    divelement.appendChild(para);
    divelement.appendChild(para1);

    Chennai.appendChild(divelement);
}

function showBranchM() {
    Mumbai.innerHTML = "";

    let divelement = document.createElement('div');

    let image = document.createElement('img');
    image.classList.add('shadow-lg', 'sa');
    image.setAttribute('src', 'https://cdn.apollohospitals.com/dev-apollohospitals/2020/09/apollo-navi-mumbai-1-1.jpg');
    image.setAttribute('alt', 'img is not found');

    let head = document.createElement('h3');
    const headTextNode = document.createTextNode("Hospitals in Mumbai");
    head.appendChild(headTextNode);

    let para = document.createElement('p');
    let icon1 = document.createElement('i');
    icon1.classList.add('fa-solid', 'fa-location-dot');
    para.appendChild(icon1);

    let paraTextNode = document.createTextNode("Parsik Hill Road, Sector 23, CBD Belapur, Navi Mumbai - 400 614");
    para.appendChild(paraTextNode);

    let para1 = document.createElement('p');
    let icon2 = document.createElement('i');
    icon2.classList.add('fa-solid', 'fa-phone');
    para1.appendChild(icon2);

    paraTextNode = document.createTextNode(" +(91)-22 3350 3350");
    para1.appendChild(paraTextNode);

    let icon3 = document.createElement('i');
    icon3.classList.add('fa-solid', 'fa-location-arrow');
    para1.appendChild(icon3);

    let icon4 = document.createElement('i');
    icon4.classList.add('fa-solid', 'fa-envelope');
    para1.appendChild(icon4);

    divelement.appendChild(image);
    divelement.appendChild(head);
    divelement.appendChild(para);
    divelement.appendChild(para1);

    Mumbai.appendChild(divelement);
}

function showBranchK() {
    Kolkata.innerHTML = "";

    let divelement = document.createElement('div');

    let image = document.createElement('img');
    image.classList.add('shadow-lg', 'sa');
    image.setAttribute('src', 'https://cdn.apollohospitals.com/dev-apollohospitals/2023/06/apollo-hospitals-kolkata-01.jpg');
    image.setAttribute('alt', 'img is not found');

    let head = document.createElement('h3');
    const headTextNode = document.createTextNode("Hospitals in Kolkata");
    head.appendChild(headTextNode);

    let para = document.createElement('p');
    let icon1 = document.createElement('i');
    icon1.classList.add('fa-solid', 'fa-location-dot');
    para.appendChild(icon1);

    let paraTextNode = document.createTextNode("58, Canal Circular Road Kolkata - 700 054");
    para.appendChild(paraTextNode);

    let para1 = document.createElement('p');
    let icon2 = document.createElement('i');
    
    icon2.classList.add('fa-solid', 'fa-phone');
    para1.appendChild(icon2);

    paraTextNode = document.createTextNode(" +91-33-2320 3040 / 2122");
    para1.appendChild(paraTextNode);

    let icon3 = document.createElement('i');
    icon3.classList.add('fa-solid', 'fa-location-arrow');
    para1.appendChild(icon3);

    let icon4 = document.createElement('i');
    icon4.classList.add('fa-solid', 'fa-envelope');
    para1.appendChild(icon4);

    divelement.appendChild(image);
    divelement.appendChild(head);
    divelement.appendChild(para);
    divelement.appendChild(para1);

    Kolkata.appendChild(divelement);
}
