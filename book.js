
function submitForm() {
    var form = document.getElementById("booking-form");
    var responseElement = document.getElementById("response");
    var name = form.elements["name"].value;
    responseElement.innerText = "Thank you, " + name + "! Your health check-up has been booked successfully.";
    form.reset();
}
